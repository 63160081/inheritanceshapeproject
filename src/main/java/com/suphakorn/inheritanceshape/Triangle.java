/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.inheritanceshape;

/**
 *
 * @author exhau
 */
public class Triangle extends Shape {

    private double calArea;

    public Triangle(double base, double height) {
        super(0, base, 0, height, 0);
        System.out.println("Triangle Created");
        calArea = (1.0 / 2) * base * height;
    }

    @Override
    public void cal() {
        super.cal();
        System.out.println("Triangle: base = " + base + "height = " + height + ", area = " + calArea);
    }
}
