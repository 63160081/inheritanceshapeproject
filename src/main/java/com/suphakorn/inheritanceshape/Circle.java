/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.inheritanceshape;

/**
 *
 * @author exhau
 */
public class Circle extends Shape {

    private double pi = 22.0 / 7;
    private double calArea;

    public Circle(double r) {
        super(r, 0, 0, 0, 0);
        System.out.println("Circle Created");
        calArea = (pi * (r * r));
    }

    @Override
    public void cal() {
        super.cal();
        System.out.println("Circle: r = " + r + ", area = " + calArea);
    }

}
