/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.inheritanceshape;

/**
 *
 * @author exhau
 */
public class TestShape {

    public static void main(String[] args) {
        Circle circle = new Circle(2);
        circle.cal();

        Triangle triangle = new Triangle(3, 5);
        triangle.cal();

        Rectangle rectangle = new Rectangle(4, 3);
        rectangle.cal();

        Square square = new Square(2);
        square.cal();
    }

}
