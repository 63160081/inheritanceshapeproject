/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.inheritanceshape;

/**
 *
 * @author exhau
 */
public class Shape {

    protected double r = 0;
    protected double base = 0;
    protected double width = 0;
    protected double height = 0;
    protected double side = 0;

    public Shape(double r, double base, double width, double height, double side) {
        this.r = r;
        this.base = base;
        this.width = width;
        this.height = height;
        this.side = side;
    }

    public void cal() {
        System.out.println("Shape Calculate");
    }

    public double getR() {
        return r;
    }

    public double getBase() {
        return base;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double getside() {
        return side;
    }

}
