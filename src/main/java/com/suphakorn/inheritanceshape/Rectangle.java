/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.inheritanceshape;

/**
 *
 * @author exhau
 */
public class Rectangle extends Shape {

    private double calArea;

    public Rectangle(double width, double height) {
        super(0, 0, width, height, 0);
        System.out.println("Rectangle Created");
        calArea = width * height;
    }

    @Override
    public void cal() {
        super.cal();
        System.out.println("Rectangle: width = " + width + "height = " + height + ", area = " + calArea);
    }
}
